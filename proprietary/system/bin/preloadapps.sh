#!/system/bin/sh
# preloadapps.sh
if [ ! -f "/data/local/tmp/preloadapps/preloadapps.lock" ]; then
echo "preloadapps ..." 
mkdir -p /data/local/tmp/preloadapps
unzip /product/zipres/preloadapps.zip -d /data/local/tmp/preloadapps/
for line in `ls /data/local/tmp/preloadapps/apk`
do
pm install -r /data/local/tmp/preloadapps/apk/$line >> /data/local/tmp/preloadapps/pmlog.txt
done
echo "preloadapps completed !" > /data/local/tmp/preloadapps/preloadapps.lock
chmod 0770 /data/local/tmp/preloadapps/preloadapps.lock
sync
fi
