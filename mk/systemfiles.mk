PRODUCT_COPY_FILES += \
    $(LINEAGEOSONREDMI9AVENDOR_PATH)/proprietary/system/etc/init/loginfo.rc:$(TARGET_COPY_OUT_SYSTEM)/etc/init/loginfo.rc \
    $(LINEAGEOSONREDMI9AVENDOR_PATH)/proprietary/system/bin/preloadapps.sh:$(TARGET_COPY_OUT_SYSTEM)/bin/preloadapps.sh \
    $(LINEAGEOSONREDMI9AVENDOR_PATH)/proprietary/system/etc/init/preloadapps.rc:$(TARGET_COPY_OUT_SYSTEM)/etc/init/preloadapps.rc \
    $(LINEAGEOSONREDMI9AVENDOR_PATH)/proprietary/system/etc/permissions/privapp-permissions_com.aefyr.sai.fdroid.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions_com.aefyr.sai.fdroid.xml \
    $(LINEAGEOSONREDMI9AVENDOR_PATH)/proprietary/system/lib/libboot_info.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libboot_info.so \
    
